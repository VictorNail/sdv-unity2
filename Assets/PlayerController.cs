using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using TMPro;
using System;
public class PlayerController : MonoBehaviour
{
    public float jumpForce = 10f; // Force du saut
    public LayerMask groundLayer; // Layer pour d�tecter le sol
    private Rigidbody2D rb;

    public float groundCheckRadius = .5f;
    public Transform groundCheck;
    public bool isGrounded;

    public TMP_Text textScore;
    public int valueScore = 0;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        textScore.text = "Score : " + valueScore;
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Jump();
        }
    }

    void Jump()
    {
        // Appliquer une force vers le haut pour effectuer le saut
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }

    public void incrScore()
    {
        valueScore += 1;
    }

}

