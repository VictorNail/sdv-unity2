using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{

    public GameObject PrefabToSpawn;
    public float SpawnRate = 3F;
    private float NextSpawn;

     void Update()
    {
        if(Time.time > NextSpawn)
        {

            NextSpawn = Time.time + SpawnRate;
            float RandomSpawn = Random.Range(1f, 5f);

            // Calculer le mouvement vers la gauche
            Vector3 movement = new Vector3(0f, RandomSpawn, 0f);
            Vector3 newPosition = transform.position + movement;

            Instantiate(PrefabToSpawn, newPosition, Quaternion.identity);
        }
    }
}
