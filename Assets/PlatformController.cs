using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    public float moveSpeed = 5F;

    // Update is called once per frame
    void Update()
    {
        MoveLeft();
        if (transform.position.x < -16)
        {
            Destroy(gameObject);
        }
    }

    void MoveLeft()
    {
        // Calculer le mouvement vers la gauche
        Vector3 movement = new Vector3(-1f, 0f, 0f);
        Vector3 newPosition = transform.position + movement * moveSpeed * Time.deltaTime;

        // Appliquer le mouvement � la plateforme en utilisant le Rigidbody
        transform.position = newPosition;

    }
}
