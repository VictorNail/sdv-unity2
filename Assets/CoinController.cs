using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    // Start is called before the first frame update
    public float moveSpeed = 5F;
    // Update is called once per frame
    void Update()
    {
        MoveLeft();
        if (transform.position.x < -16)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Destroy(gameObject);
        collider.gameObject.GetComponent<PlayerController>().incrScore();

    }

    void MoveLeft()
    {
        // Calculer le mouvement vers la gauche
        Vector3 movement = new Vector3(-1f, 0f, 0f);
        Vector3 newPosition = transform.position + movement * moveSpeed * Time.deltaTime;

        // Appliquer le mouvement � la plateforme en utilisant le Rigidbody
        transform.position = newPosition;

    }
}
